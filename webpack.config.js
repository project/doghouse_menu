const { createConfig } = require('@doghouse/webpack-base');
const path = require('path');

// Paths
// -------------------------
const absPath = path.resolve(__dirname);

// // Add export config.
// // -------------------------
// // Each object added to this should be wrapped by createConfig() to merge in base config.
// // Eg. module.exports.push(createConfig({ ... })
module.exports = [];

module.exports.push(createConfig({
  entry: {
    // Entry point for SCSS to compile.
    'doghouse-menu': [absPath + '/css/src/doghouse-menu.scss'],
    'region-doghouse-menu': [absPath + '/css/src/region-doghouse-menu.scss'],
  },
  output: {
    // Dir that CSS gets saved to.
    path: absPath + '/css/dist',
    publicPath: ''
  }
}));

module.exports.push(createConfig({
  entry: {
    // Entry point for SCSS to compile.
    'doghouse-megamenu': [absPath + '/modules/doghouse_megamenu/css/src/doghouse-megamenu.scss'],
  },
  output: {
    // Dir that CSS gets saved to.
    path: absPath + '/modules/doghouse_megamenu/css/dist',
    publicPath: ''
  }
}));

module.exports.push(createConfig({
  entry: {
    'doghouse-menu-toggle': [absPath + '/js/src/doghouse-menu-toggle.js'],
  },
  output: {
    path: absPath + '/js/dist',
    filename: '[name].js',
  }
}));

module.exports.push(createConfig({
  entry: {
    'doghouse-menu': [absPath + '/js/src/doghouse-menu.js'],
  },
  output: {
    path: absPath + '/js/dist',
    filename: '[name].js',
  }
}));

<?php

namespace Drupal\doghouse_megamenu\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Menu\MenuLinkTreeInterface;
use Drupal\Core\Menu\MenuTreeStorageInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'DoghouseMegaMenuBlock' block.
 *
 * @Block(
 *  id = "doghouse_mega_menu_block",
 *  admin_label = @Translation("Doghouse mega menu block"),
 * )
 */
class DoghouseMegaMenuBlock extends BlockBase implements ContainerFactoryPluginInterface {

    /**
     * Drupal\Core\Menu\MenuLinkTreeInterface definition.
     *
     * @var \Drupal\Core\Menu\MenuLinkTreeInterface
     */
    protected $menuLinkTree;

    /**
     * Drupal\Core\Menu\MenuTreeStorageInterface definition.
     *
     * @var \Drupal\Core\Menu\MenuTreeStorageInterface
     */
    protected $menuTreeStorage;

    /**
     * @var \Drupal\Core\Entity\EntityTypeManager
     */
    private $entityTypeManager;

    /**
     * Constructs a new DoghouseMegaMenuBlock object.
     *
     * @param array $configuration
     *   A configuration array containing information about the plugin instance.
     * @param string $plugin_id
     *   The plugin_id for the plugin instance.
     * @param string $plugin_definition
     *   The plugin implementation definition.
     * @param \Drupal\Core\Menu\MenuLinkTreeInterface $menu_link_tree
     *   The MenuLinkTreeInterface definition.
     * @param \Drupal\Core\Menu\MenuTreeStorageInterface $menu_tree_storage
     *   The MenuTreeStorageInterface definition.
     * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
     */
    public function __construct(
        array $configuration,
        $plugin_id,
        $plugin_definition,
        MenuLinkTreeInterface $menu_link_tree,
        MenuTreeStorageInterface $menu_tree_storage,
        EntityTypeManager $entity_type_manager
    ) {
        parent::__construct($configuration, $plugin_id, $plugin_definition);
        $this->menuLinkTree = $menu_link_tree;
        $this->menuTreeStorage = $menu_tree_storage;
        $this->entityTypeManager = $entity_type_manager;
    }

    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
        return new static(
            $configuration,
            $plugin_id,
            $plugin_definition,
            $container->get('menu.link_tree'),
            $container->get('menu.tree_storage'),
            $container->get('entity_type.manager')
        );
    }

    /**
     * {@inheritdoc}
     */
    public function defaultConfiguration() {
        return [
            'menu' => '',
        ] + parent::defaultConfiguration();
    }

    /**
     * {@inheritdoc}
     */
    public function blockForm($form, FormStateInterface $form_state) {
        $menuOptions = $this->entityTypeManager->getStorage('menu')->getQuery()->execute();
        $form['menu'] = [
            '#type' => 'select',
            '#title' => $this->t('Menu'),
            '#options' => $menuOptions,
            '#default_value' => $this->configuration['menu'],
        ];

        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function blockSubmit($form, FormStateInterface $form_state) {
        $this->configuration['menu'] = $form_state->getValue('menu');
    }

    private function loadMenuTrue() {
        $menu_name = $this->configuration['menu'];
        $parameters = $this->menuLinkTree->getCurrentRouteMenuTreeParameters($menu_name);
        $tree = $this->menuLinkTree->load($menu_name, $parameters);
        $manipulators = [
            ['callable' => 'menu.default_tree_manipulators:checkAccess'],
            ['callable' => 'menu.default_tree_manipulators:generateIndexAndSort'],
        ];
        return $this->menuLinkTree->transform($tree, $manipulators);
    }

    public function buildMegaMenu($tree, $builtMenu) {
        $menu = [
            '#theme' => 'doghouse_mega_menu_block',
            '#attributes' => [
                'class' => [
                    'doghouse-megamenu',
                ],
            ],
            '#items' => [],
        ];

        foreach ($tree as $dataMenu => $tree_element) {
            $link = $tree_element->link;
            $options = $link->getOptions();
            $id = $link->getPluginId();
            $definition = $tree_element->link->getPluginDefinition();

            // Skip links that are disabled.
            if ($definition['enabled'] == 0) {
                continue;
            }

            $menu['#items'][$id] = [
                'menu_item' => $builtMenu['#items'][$id],
            ];

            if (!empty($options['megamenu_block_id'])) {
                $block = $this->entityTypeManager->getStorage('block_content')->load($options['megamenu_block_id']);
                $rendered = $this->entityTypeManager->getViewBuilder($block->getEntityTypeId())->view($block, 'full');
                $menu['#items'][$id]['block'] = $rendered;
            }
        }

        return $menu;
    }

    /**
     * {@inheritdoc}
     */
    public function build() {
        $tree = $this->loadMenuTrue();
        $builtMenu = $this->menuLinkTree->build($tree);
        foreach ($builtMenu['#items'] as &$item) {
            $options = $item['url']->getOptions();
            $options = array_merge(['attributes' => []], $options);
            $item['url']->setOptions($options);
        }
        $megamenu = $this->buildMegaMenu($tree, $builtMenu);
        return $megamenu;
    }

}

<?php

namespace Drupal\doghouse_menu\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'MenuToggle' block.
 *
 * @Block(
 *  id = "doghouse_menu_toggle",
 *  admin_label = @Translation("Doghouse Menu Toggle"),
 * )
 */
class MenuToggle extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
        'toggle_text' => 'Menu Toggle',
      ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['toggle_text'] = [
      '#type' => 'textfield',
      '#title' => 'Toggle Button Text',
      '#description' => $this->t('The text to display in the toggle button'),
      '#default_value' => $this->configuration['toggle_text'],
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['toggle_text'] = $form_state->getValue('toggle_text');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [
      '#attributes' => [
        'class' => ['doghouse-menu-toggle'],
      ],
      '#attached' => [
        'library' => [
          'doghouse_menu/doghouse_menu_toggle',
        ],
      ],
    ];
    $build['menu_toggle'] = [
      '#theme' => 'doghouse_menu_toggle',
      '#text' => $this->configuration['toggle_text'],
    ];

    return $build;
  }

}

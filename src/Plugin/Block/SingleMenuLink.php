<?php

namespace Drupal\doghouse_menu\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Menu\MenuLinkTreeInterface;
use Drupal\Core\Menu\MenuTreeStorageInterface;
use Drupal\Core\Url;

/**
 * Provides a 'SingleMenuLink' block.
 *
 * @Block(
 *  id = "single_menu_link",
 *  admin_label = @Translation("Single menu link"),
 * )
 */
class SingleMenuLink extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Drupal\Core\Menu\MenuLinkTreeInterface definition.
   *
   * @var \Drupal\Core\Menu\MenuLinkTreeInterface
   */
  protected $menuLinkTree;

  /**
   * Drupal\Core\Menu\MenuTreeStorageInterface definition.
   *
   * @var \Drupal\Core\Menu\MenuTreeStorageInterface
   */
  protected $menuTreeStorage;

  /**
   * Constructs a new SingleMenuLink object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param string $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Menu\MenuLinkTreeInterface $menu_link_tree
   *   Defines an interface for loading, transforming and rendering menu link trees.
   * @param \Drupal\Core\Menu\MenuTreeStorageInterface $menu_tree_storage
   *   Defines an interface for storing a menu tree containing menu link IDs.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    MenuLinkTreeInterface $menu_link_tree,
    MenuTreeStorageInterface $menu_tree_storage
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->menuLinkTree = $menu_link_tree;
    $this->menuTreeStorage = $menu_tree_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('menu.link_tree'),
      $container->get('menu.tree_storage')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
        'menu_link' => '',
      ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {

    $form['menu_link'] = [
      '#type' => 'select',
      '#title' => $this->t('Menu Link'),
      '#description' => $this->t('Select a menu link from the main menu to display'),
      '#options' => $this->buildMenuLinkOptions(),
      '#required' => TRUE,
      '#default_value' => $this->configuration['menu_link'],
    ];

    return $form;
  }

  /**
   * Build an array of select list options.
   *
   * @return array
   */
  public function buildMenuLinkOptions() {
    $menu_name = 'main';
    $parameters = $this->menuLinkTree->getCurrentRouteMenuTreeParameters($menu_name);
    $tree = $this->menuLinkTree->load($menu_name, $parameters);
    $options = [];
    foreach ($tree as $id => $menuLinkTreeElement) {
      $link = $menuLinkTreeElement->link;
      $options[$id] = $link->getTitle();
    }
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['menu_link'] = $form_state->getValue('menu_link');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    $hasperm = \Drupal::currentUser()->hasPermission('administer blocks provided by doghouse_menu');

    $build = [];

    if (!empty($this->configuration['menu_link'])) {
      $link = $this->menuTreeStorage->load($this->configuration['menu_link']);
      if ($link) {
        $link['options']['attributes']['class'][] = 'single-menu-item';
        $url = Url::fromUri($link['url']);
        $url->mergeOptions(['attributes' => $link['options']['attributes']]);

        $build = [
          '#type' => 'link',
          '#title' => $link['title'],
          '#url' => $url,
        ];
      }
    }
    elseif (\Drupal::currentUser()->hasPermission('administer blocks provided by doghouse_menu')) {
      $build = [
        '#markup' => 'please configure this block and provide a menu link',
      ];
    }

    return $build;
  }

}

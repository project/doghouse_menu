<?php

namespace Drupal\doghouse_menu\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'MenuClose' block.
 *
 * @Block(
 *  id = "doghouse_menu_close",
 *  admin_label = @Translation("Doghouse Menu Close"),
 * )
 */
class MenuClose extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
        'close_text' => 'Close',
      ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['close_text'] = [
      '#type' => 'textfield',
      '#title' => 'Button Text',
      '#description' => $this->t('The text to display in the button'),
      '#default_value' => $this->configuration['close_text'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['close_text'] = $form_state->getValue('close_text');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [
      'close' => [
        '#theme' => 'doghouse_menu_close',
        '#text' => $this->configuration['close_text'],
      ],
      '#attached' => [
        'library' => [
          'doghouse_menu/doghouse_menu_toggle',
        ],
      ],
      '#attributes' => [
        'class' => ['doghouse-menu-close']
      ],
    ];

    return $build;
  }

}

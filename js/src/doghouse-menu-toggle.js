(function ($, Drupal, drupalSettings) {

  // const toggleClass = 'doghouse-menu-toggle-open';
  const toggleClass = 'doghouse-menu--open';

  Drupal.behaviors.doghouseMenuToggle = {
    attach(context) {
      $('.js-doghouse-menu-toggle', context).once('menu-toggle').click(function(e) {
        e.preventDefault();
        $('body').toggleClass(toggleClass);
      });
    },
  };

  Drupal.behaviors.doghouseMenuClose = {
    attach(context) {
      $('.js-doghouse-menu-close', context).once('menu-close').click(function(e) {
        e.preventDefault();
        $('body').removeClass(toggleClass);
      });
    },
  };

}(jQuery, Drupal, drupalSettings));

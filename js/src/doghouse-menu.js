/**
 * @file
 * Defines Javascript behaviors for the doghouse menu.
 */

 import {SlideMenu, AccordionMenu} from './Menu';

(function ($, Drupal, drupalSettings) {

  Drupal.doghouseMenu = Drupal.doghouseMenu || [];

  /**
   * Behaviors for summaries for tabs in the media edit form.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches summary behavior for tabs in the media edit form.
   */
  Drupal.behaviors.doghouseMenu = {
    attach(context) {
      $('.js-doghouse-menu', context).once('doghouse-menu').doghouseMenu();
    },
  };

  $.fn.doghouseMenu = function (options = {}) {
    return this.each(function(i, v) {
      options.el = $(v);
      switch (options.el.attr('data-menu-type')) {
        case 'slide':
            Drupal.doghouseMenu.push(new SlideMenu(options));
            break;

        case 'accordion':
            Drupal.doghouseMenu.push(new AccordionMenu(options));
            break;
      }
    });
  };


}(jQuery, Drupal, drupalSettings));
